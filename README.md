# Simple_AWS_Lambda_Function
# Brownian Motion Simulator

Tianji Rao

## Introduction
In this project, the author build a Brownian Motion simulator as a AWS lambda function based on Rust and cargo lambda.
Using this function we can input total time T and number of steps, and it will return a list of brownian motion increment. 
As a technical solution, the author utilized the function of AWS lambda, AWS API Gateway, data prerpocessing and Gitlab. 
This AWS Lambda function can be helpful for option pricing with Monte-carlo simultion, where we need the Brownian motion simulator to map stochastic processes.
Here, we also add Logging and Tracing

## Diagram of Lambda Function

## Cloud Watch and X-ray

![Screenshot_2024-03-06_at_11.00.43_PM](ss1.png)

![Screenshot_2024-03-06_at_11.14.44_PM](ss2.png)


## Setup
1. Install Rust and Cargo Lambda: `brew install rust`, `brew tap cargo-lambda/cargo-lambda`, `brew install cargo-lambda`    
2. Build a Cargo project:   
- Build your cargo lambda function based on the templet 
- Test locally with `watch` and `invoke`: Using these methods, we can test if the request works well and how the return is.  
3. AWS set-up:  
- Create an AWS account 
- AWS IAM web management: add users, attach polices, and set access keys.   
- AWS Lambda: release the function, and deploy.     
- AWS API Gateway: create an app, set API.  
4. Gitlab set-up:
- Add secrets: add AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION to gitlab secrets
- Check Cloud Watch and AWS X-Ray

## References
- https://stratusgrid.com/blog/aws-lambda-rust-how-to-deploy-aws-lambda-functions
- https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-create-api-as-simple-proxy-for-lambda.html
- https://www.cobbing.dev/blog/grappling-a-rust-lambda/
- https://www.cargo-lambda.info/commands/deploy.html
- https://coursera.org/groups/building-rust-aws-lambda-microservices-with-cargo-lambda-rd2pc
- https://github.com/DaGenix/rust-crypto/blob/master/examples/symmetriccipher.rs
