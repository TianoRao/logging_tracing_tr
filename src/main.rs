use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use aws_lambda_log_trace::{simulate};
use tracing::{info, warn};

#[derive(Deserialize)]
struct Request {
    time: f64,
    num_steps: usize,
}

#[derive(Serialize)]
struct Response {
    msg: Vec<f64>,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let num_steps = event.payload.num_steps;
    let time = event.payload.time;

    // Log the received request
    info!("Received request: time = {}, num_steps = {}", time, num_steps);

    // Call the simulate function from your library
    let simulated_values = simulate(time, num_steps);

    // Check the simulation result and log accordingly
    if simulated_values.is_empty() {
        warn!("Simulation returned no results. This might indicate an issue with the input parameters or the simulation logic.");
    } else {
        info!("Simulation completed successfully with {} steps", simulated_values.len());
    }

    // Create a response with the simulated values
    let resp = Response { msg: simulated_values };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with_target(false) // Disable printing the name of the module in every log line
        .without_time() // Disable time because CloudWatch adds the ingestion time
        .init();

    run(service_fn(function_handler)).await
}
