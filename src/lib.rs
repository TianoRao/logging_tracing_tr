use rand_distr::{Normal, Distribution};

pub fn simulate(time: f64, num_steps: usize) -> Vec<f64> {
    let mut rng = rand::thread_rng();
    let normal = Normal::new(0.0, (time / num_steps as f64).sqrt()).unwrap();
    
    (0..num_steps).map(|_| normal.sample(&mut rng)).collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_simulate() {
        let time = 10.0;
        let num_steps = 5;
        let result = simulate(time, num_steps);
        assert_eq!(result.len(), num_steps);
        // Add more tests as needed
    }
}
